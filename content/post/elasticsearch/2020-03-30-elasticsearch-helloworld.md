+++
title='认识 Elasticsearch'
tags=[]
categories=["elasticsearch"]
date="2020-03-30T11:02:34+08:00"
draft=false
toc=true
+++

ElasticSearch是一个基于Lucene的搜索服务器,它提供了一个分布式多用户能力的全文搜索引擎,基于RESTful web接口.Elasticsearch是用Java语言开发的,
并作为Apache许可条款下的开放源码发布,是一种流行的企业级搜索引擎．

<!--more-->

## 安装elasticsearch

#### 使用docker安装
拉取elasticsearch镜像
```bash
sudo docker pull elasticsearch:6.4.0

```
修改vm.max_map_count参数
```bash
sudo sysctl -w vm.max_map_count=262144

```
启动容器elasticsearch
```bash
 sudo docker run -p 9200:9200 -p 9300:9300 --name elasticsearch \
 -e "discovery.type=single-node" \
 -e "cluster.name=elasticsearch" \
 -v /mydata/elasticsearch/plugins:/usr/share/elasticsearch/plugins \
 -v /mydata/elasticsearch/data:/usr/share/elasticsearch/data \
 -d elasticsearch:6.4.0
```
进入容器
```bash
sudo docker exec -it elasticsearch /bin/bash

```
如果进入容器报错,修改数据文件权限,重启
```bash
sudo chmod +777 /mydata/elasticsearch/data
sudo docker restart elasticsearch
```
测试是否启动成功
```bash
curl  localhost:9200

```

## 基本概念

#### node和cluster
单个elasticsearch实例称为一个节点,一组节点构成为一个集群cluster
#### index(索引)
index是es数据管理的顶层单位,相当于mysql的数据库.在es中还有一个名词是`倒序索引`,关系型数据库通过增加一个 索引 比如一个 B树（B-tree）
索引 到指定的列上,以便提升数据检索速度.Elasticsearch 和 Lucene 使用了一个叫做 倒排索引 的结构来达到相同的目的. 默认的，一个文档中的
每一个属性都是 被索引 的（有一个倒排索引）和可搜索的.一个没有倒排索引的属性是不能被搜索到的.
#### document(文档)
document是index里面存储的单条记录,同一个index里面不要求所有的document结构相同

## 和Elasticsearch进行交互
使用http请求的方式和elasticsearch进行交互
```bash
curl -X<VERB> -H "Content-Type: application/json;charset=UTF-8" '<PROTOCOL>://<HOST>:<PORT>/<PATH>?<QUERY_STRING>' -d '<BODY>'

```

| 变量 | 含义 |
| -------- | ------------------------------------ |
| VERB | 适当的 HTTP 方法 或 谓词 : GET、 POST、 PUT、 HEAD 或者 DELETE. |
| PROTOCOL | http 或者 https（如果你在 Elasticsearch 前面有一个 https 代理） |
| HOST | Elasticsearch 集群中任意节点的主机名,或者用 localhost 代表本地机器上的节点 |
| PORT | 运行 Elasticsearch HTTP 服务的端口号,默认是 9200 |
| PATH | API 的终端路径（例如 _count 将返回集群中文档数量）。Path 可能包含多个组件,例如：_cluster/stats 和 _nodes/stats/jvm |
| QUERY_STRING | 任意可选的查询字符串参数 (例如 ?pretty 将格式化地输出 JSON 返回值,使其更容易阅读) |
| BODY | 一个 JSON 格式的请求体 (如果请求需要的话) |

## 向索引中添加文档
向company索引中添加employee类型的文档.
```bash
curl -XPUT -H "Content-Type:application/json;charset=UTF-8" http://localhost:9200/company/employee/1 -d '
{                         
    "first_name" : "John",
    "last_name" :  "Smith",
    "age" :        25,
    "about" :      "I love to go rock climbing",
    "interests": [ "sports", "music" ]
}'
```
company为索引的名称,employee为文档的类型,1为文档的id.

添加更多的employee
```bash
curl -XPUT -H "Content-Type:application/json;charset=UTF-8" http://localhost:9200/company/employee/2 --d'
{
    "first_name" :  "Jane",
    "last_name" :   "Smith",
    "age" :         32,
    "about" :       "I like to collect rock albums",
    "interests":  [ "music" ]
}'

curl -XPUT -H "Content-Type:application/json;charset=UTF-8" http://localhost:9200/company/employee/3 --d '
{
    "first_name" :  "Douglas",
    "last_name" :   "Fir",
    "age" :         35,
    "about":        "I like to build cabinets",
    "interests":  [ "forestry" ]
}'
```
## 简单文档检索
查询id为1的employee
```bash
curl http://localhost:9200/company/employee/1

```
返回结果:
```json
{"_index":"company","_type":"employee","_id":"1","_version":1,"found":true,"_source":
{
    "first_name" : "John",
    "last_name" :  "Smith",
    "age" :        25,
    "about" :      "I love to go rock climbing",
    "interests": [ "sports", "music" ]
}}
```

查询所有的employee
```bash
curl http://localhost:9200/company/employee/_search?pretty

```
返回结果(一个搜索默认返回十条结果):
```json
{
  "took" : 1,
  "timed_out" : false,
  "_shards" : {
    "total" : 5,
    "successful" : 5,
    "skipped" : 0,
    "failed" : 0
  },
  "hits" : {
    "total" : 3,
    "max_score" : 1.0,
    "hits" : [
      {
        "_index" : "company",
        "_type" : "employee",
        "_id" : "2",
        "_score" : 1.0,
        "_source" : {
          "first_name" : "Jane",
          "last_name" : "Smith",
          "age" : 32,
          "about" : "I like to collect rock albums",
          "interests" : [
            "music"
          ]
        }
      },
      {
        "_index" : "company",
        "_type" : "employee",
        "_id" : "1",
        "_score" : 1.0,
        "_source" : {
          "first_name" : "John",
          "last_name" : "Smith",
          "age" : 25,
          "about" : "I love to go rock climbing",
          "interests" : [
            "sports",
            "music"
          ]
        }
      },
      {
        "_index" : "company",
        "_type" : "employee",
        "_id" : "3",
        "_score" : 1.0,
        "_source" : {
          "first_name" : "Douglas",
          "last_name" : "Fir",
          "age" : 35,
          "about" : "I like to build cabinets",
          "interests" : [
            "forestry"
          ]
        }
      }
    ]
  }
}
```

查询姓氏为Simth的雇员
```bash
curl http://localhost:9200/company/employee/_search?q=last_name=Smith

```
返回结果:
```json
{
    "took": 4,
    "timed_out": false,
    "_shards": {
        "total": 5,
        "successful": 5,
        "skipped": 0,
        "failed": 0
    },
    "hits": {
        "total": 2,
        "max_score": 0.2876821,
        "hits": [
            {
                "_index": "company",
                "_type": "employee",
                "_id": "2",
                "_score": 0.2876821,
                "_source": {
                    "first_name": "Jane",
                    "last_name": "Smith",
                    "age": 32,
                    "about": "I like to collect rock albums",
                    "interests": [
                        "music"
                    ]
                }
            },
            {
                "_index": "company",
                "_type": "employee",
                "_id": "1",
                "_score": 0.2876821,
                "_source": {
                    "first_name": "John",
                    "last_name": "Smith",
                    "age": 25,
                    "about": "I love to go rock climbing",
                    "interests": [
                        "sports",
                        "music"
                    ]
                }
            }
        ]
    }
}
```