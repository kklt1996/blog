+++
title='docker安装mysql实验环境'
tags=['docker']
categories=['mysql']
date="2020-04-06T13:58:38+08:00"
toc=true
draft=false
+++

本文将介绍mysql的安装,如何搭建实验性的mysql环境.
<!--more-->

docker是基于linux的轻量级的虚拟化技术,能够秒级的启动容器服务.使用docker搭建mysql的学习环境是方便快捷的.方便我们在本地做一些问题的验证.

## 安装docker
首先要在本地安装docker服务

## 使用docker启动mysql
使用hub.c.163.com/library/mysql:5.7镜像启动名称为mysql5.7的容器服务,mysql容器服务root账户的密码设置为root
```shell script
sudo docker run --name mysql5.7 -p 3306:3306 -e MYSQL_ROOT_PASSWORD=root -d hub.c.163.com/library/mysql:5.7

```
查看启动日志
```shell script
sudo docker logs mysql5.7

```

## 进入mysql容器服务
在mysql5.7容器的内部执行bash命令
```shell script
sudo docker exec -it mysql5.7 bash

```

## 连接mysql服务
进入mysql5.7容器内部之后,连接容器内部的mysql服务
```shell script
mysql -u root -p

```
输入root账户的密码:root

## 关闭mysql服务
```shell script
sudo docker stop mysql5.7

```

## 启动mysql服务
```shell script
sudo docker start mysql5.7

```

## 重启mysql服务
```shell script
sudo docker restart mysql5.7

```


