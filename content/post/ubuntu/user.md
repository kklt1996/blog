---
title: ubuntu16.04中添加删除用户
date: 2017-11-24
tags: [防火墙]
categories: [ubuntu]
---
本篇文章将介绍如何使用在ubuntu16.04中添加删除用户
<!--more-->

## 添加用户，带用户文件
创建用户
```bash
useradd -r -m -s /bin/bash spark 
```
设置密码
```bash
passwd spark
```
赋予用户root权限
```bash
vim /etc/sudoers
```
添加　
```bash
spark ALL=(ALL:ALL) ALL
```
##　删除用户
```bash
userdel -r spark 
```
