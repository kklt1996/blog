---
title: ubuntu16.04中设置缓冲区以及缓冲区使用策略
date: 2017-11-24
tags: [swap]
categories: [ubuntu]
toc: true
---
本篇文章将介绍如何使用在ubuntu16.04中设置缓冲区以及配置缓冲区使用策略
<!--more-->
## 创建缓冲区文件
创建一个大小为4G的缓冲区文件
```bash
sudo fallocate -l 4G /swapfile
```
## 赋予权限Swap分区文件权限
```bash
sudo chmod 600 /swapfile
```
## 将swapfile初始化为交换文件
```bash
sudo mkswap /swapfile
```
## 启用交换文件
```bash
sudo swapon /swapfile
```
## 查看分区
```bash
free -m 
```
## 设置开机启用
编辑/etc/fstab文件
```bash
sudo vim /etc/fstab
```
在文件末尾添加如下内容
```bash
/swapfile none swap sw 0 0
```
## 卸载swap分区
```bash
sudo swapoff /swapfile
```
## 设置缓冲区使用策略
1.修改vm.swappiness的值,使用所有虚拟内存中缓冲区所占百分比
```bash
sudo sysctl vm.swappiness=40
```
2.修改/etc/sysctl.conf配置文件
```bash
vim /etc/sysctl.conf
```
在文件尾部添加vm.swappiness=40,然后重启ubuntu生效