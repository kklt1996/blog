---
title: ubuntu16.04中开启和关闭防火墙
date: 2017-11-24
tags: [防火墙]
categories: [ubuntu]
toc: true
---
本篇文章将介绍如何使用在ubuntu16.04中开启和关闭防火墙
<!--more-->

## 开启防火墙
ufw enable
## 关闭防火墙
ufw disable