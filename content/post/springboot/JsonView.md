---
title: springBoot中使用jackjson定义多个视图
date: 2017-10-18
tags: ["jackjson"]
categories: ["json"]
toc: true
---
本篇文章将介绍如何jackjson的JsonView注解定义多个视图，在springmvc的返回值中返回特定的视图
<!--more-->
## 在pojo上定义多个视图
```java
import com.fasterxml.jackson.annotation.JsonView;
import org.hibernate.validator.constraints.*;

import javax.validation.constraints.*;
import java.util.Date;

/**
 * 使用jsonView和接口来声明多个视图
 * 这个pojo上定义了两个视图一个是SimpleView，一个是DetailView，
 * SimpleView视图中只有一个name,DetailView继承自SimpleView,所以他拥有
 * Simpleiew的所有属性
 */
public class User {


  public  interface SimpleView{};
  public interface DetailView extends SimpleView{};

  @JsonView(SimpleView.class)
  private String name;

  @JsonView(DetailView.class)
  private String password;

  private Date birthday;

  public User() {
  }

  public User(String name) {
    this.name = name;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getPassword() {
    return password;
  }

  public void setPassword(String password) {
    this.password = password;
  }
  public Date getBirthday() {
    return birthday;
  }

  public void setBirthday(Date birthday) {
    this.birthday = birthday;
  }
}
```
## 在SpringMvc的方法之上指明使用哪个视图，如果不指明使用的视图的话返回值将显示所有的字段
```java
import com.fasterxml.jackson.annotation.JsonView;
import com.micro.fast.security.demo.pojo.User;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/user")
public class RestfulController {

  @GetMapping
  @JsonView(User.SimpleView.class)//指明使用那个视图
  public List<User> query(@RequestParam("username") String username){
    List<User> users = new ArrayList<>();
    users.add(new User("name"));
    users.add(new User("name"));
    users.add(new User("name"));
    return users;
  }
}
```