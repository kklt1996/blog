---
title: Springboot中hibernate参数校验,自定义校验注解
date: 2017-10-18
tags: [hibernate,validator]
categories: [springBoot]
toc: true
---
使用hibernate的校验框架进行参数校验,并自定义校验注解和逻辑
<!--more-->
## hibernate校验示例
使用校验注解配合`＠Valid`注解绑定SpringMvc的参数进行参数校验
### pojo类上加校验注解
```java
import org.hibernate.validator.constraints.*;

import javax.validation.constraints.*;
import java.util.Date;

public class User {


  private String name;
  //校验密码不能为空
  @NotBlank(message = "用户密码不能为空")//参数校验，使用message自定义校验不通过的时候返回的错误信息
  private String password;

  private Date birthday;

  public User() {
  }

  public User(String name) {
    this.name = name;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getPassword() {
    return password;
  }

  public void setPassword(String password) {
    this.password = password;
  }
   
  public Date getBirthday() {
      return birthday;
    }
  
  public void setBirthday(Date birthday) {
      this.birthday = birthday;
    }
}
```
### 在Springmvc的参数上添加＠Valid注解进行参数的校验

```java
package com.micro.fast.security.demo.controller;

import com.micro.fast.security.demo.pojo.User;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/user")
public class RestfulController {


  /**
   * @Valid配合pojo上的注解进行校验不写这个注解的话就不会执行校验逻辑,如果不写BindingResult参数的话，
   * 参数校验不合法的话就不会进入方法体里面，直接报400错误
   * @param user
   * @param errors
   * @return
   */
  @PostMapping
  public User createUser(@Valid @RequestBody User user, BindingResult errors){
    if (errors.hasErrors()){
      //如果有错误信息的话就打印出错误信息
      errors.getAllErrors().stream().forEach(error->{
        System.out.println(error.getDefaultMessage());
        FieldError fieldError = (FieldError) error;
        //错误中输出字段名字
        String s = fieldError.getField() + error.getDefaultMessage();
        //输出检验错误的字段的名字
        System.out.println(s);
      });
    }
    System.out.println(user.getName());
    System.out.println(user.getPassword());
    System.out.println(user.getBirthday());
    return user;
  }


}
```

## 常见的校验注解介绍
|注解名称| 作用| 
|:--------:|:-----:| 
|@NotBlank| 检验字符串参数不能为空|  
|@NotNull| 校验参数不能为null|  
|@NotEmpty| 字符串不能为空，集合不能为空 |   
|@Size(min = 1,max = 20)| 检验集合元素的个数是否满足要求|
|@Email|检验参数是否是邮箱格式|
|@Pattern(regexp = "a{0,1}")|使用正则表达式校验字符串|
|@CreditCardNumber()|是否是美国的信用卡号|
|@Length(min = 1,max = 100)|校验字符串的长度是否满足要求|
|@Range(min = 1,max = 2)|校验数字的值|
|@SafeHtml|校验字符串是否是安全的html|
|@URL|校验url是否是合法的url|
|@AssertFalse|校验值是否是false|
|@AssertTrue|校验值是否是true|
|@DecimalMax(value = "1.00",inclusive = true)|校验数字或者是字符串是否小于等于某个值，inclusive为false的时候为小于|
|@DecimalMin(value = "2.00",inclusive = false)|校验数字或者是字符串是否大于等于某个值，inclusive为false的时候为大于|
|@Digits(integer = 1,fraction = 2)|校验数字的格式　integer指定整数部分的长度 fraction指定小数部分的长度|
|@Past|日期必须是过去的日期|
|@Future|日期必须是未来的日期|
|@Max(value = 1)|小于等于，不能注解在字符串上|
|@Min(2)|大于等于，不能注解在字符串上|
## 自定义校验注解
### 定义校验的逻辑类
```java
import org.springframework.stereotype.Component;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

/**
 * MyContraint 校验注解的类型
 * String　要对什么类型的字段进行校验
 */
@Component
public class MyConstrainValidator implements ConstraintValidator<MyContraint,String> {

  //@Autowired 注入Service
  //private String name;
  @Override
  public void initialize(MyContraint myContraint) {
    System.out.println("初始化校验器");
  }

  @Override
  public boolean isValid(String s, ConstraintValidatorContext constraintValidatorContext) {
  //校验的逻辑
    return false;
  }
}
```
### 定义校验注解指明校验逻辑类
```java
import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

/**
 * 自定义注解
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.FIELD,ElementType.METHOD})
@Inherited
@Documented
@Constraint(validatedBy = MyConstrainValidator.class)//指明校验逻辑的类
public @interface MyContraint {
  String message() ;//校验的失败的时候返回的信息,可以指定默认值

  Class<?>[] groups() default {};

  Class<? extends Payload>[] payload() default {};
}
```

