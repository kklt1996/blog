---
title: Springboot中restful-api简介，代码测试
date: 2017-10-18
tags: ["resultful"]
categories: ["springBoot"]
toc: true
---
RESTful风格api介绍,以及如何进行测试．
<!--more-->
## restful API风格介绍
查询－>GET－>/user
详情－>GET－>/user/1
创建－>POST－>/user
修改－>PUT－>/user/1
删除－> DELETE－>/user/1
1.用URL描述资源
2.用HTTP方法描述行为，使用HTTP状态码来表示不同的结果
3.使用json交换数据
4.RESTful只是一种风格，并不是强制的标准
## restful风格的代码测试
使用junit进行代码测试
```java
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;

@RunWith(SpringRunner.class)
@SpringBootTest
public class BootStarterSecurityDemoApplicationTests {

	@Test
	public void contextLoads() {
	}

	@Autowired
	private WebApplicationContext wac;

	private MockMvc mockMvc;

	/**
	 * 在每次测试执行前构建mvc环境
	 */
	@Before
	public void setup(){
		mockMvc = MockMvcBuilders.webAppContextSetup(wac).build();
	}

  /**
   * 用户信息查询
   */
	@Test
	public void whenQuerySuccess(){
	  //测试接口的返回结果是否满足预期
		try {
			mockMvc.perform(MockMvcRequestBuilders.get("/user").param("username","pojo")
          .contentType(MediaType.APPLICATION_JSON_UTF8))
          .andExpect(MockMvcResultMatchers.status().isOk())//检查状态码
          .andExpect(MockMvcResultMatchers.jsonPath("$.length()").value(3));//检查集合的长度是３个
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

  /**
   * 获取用户的信息详情
   */
	@Test
	public void whenGetInfoSuccess(){
    try {
      mockMvc.perform(MockMvcRequestBuilders.get("/user/1")
          .contentType(MediaType.APPLICATION_JSON_UTF8))
          .andExpect(MockMvcResultMatchers.status().isOk())
          .andExpect(MockMvcResultMatchers.jsonPath("$.name").value("tom"));
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

	/**
	 * 创建用户
	 */
	@Test
  public void whenCreateSuccess(){
		//前台传入的是长整形
		Date date = new Date();
		String content = "{\"name\":\"tom\",\"password\":\"\",\"birthday\":"+date.getTime()+"}";
		try {
			String tom = mockMvc.perform(MockMvcRequestBuilders.post("/user")
					.contentType(MediaType.APPLICATION_JSON_UTF8).content(content))
					.andExpect(MockMvcResultMatchers.status().isOk())
					.andExpect(MockMvcResultMatchers.jsonPath("$.name").value("tom"))
					.andReturn().getResponse().getContentAsString();
			//后台返回的日期格式也是长整形
			System.out.println(tom);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}

```