---
title: SpringBoot中定义intercepter
date: 2017-10-21
tags: ["interceptor","intercept"]
categories: ["springBoot"]
toc: true
---
本篇文章中将介绍如何在springBoot中定义interceptor
<!--more-->
## 定义interceptor
实现`HandlerInterceptor`接口
```java
import org.springframework.stereotype.Component;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 不能获取到处理请求方法中的参数的值
 */
@Component
public class MyInterceptor implements HandlerInterceptor {
  //请求之前
  @Override
  public boolean preHandle(HttpServletRequest httpServletRequest,
                           HttpServletResponse httpServletResponse,
                           Object handler)
      throws Exception {
    //获取请求的类
    System.out.println(((HandlerMethod) handler).getBean().getClass().getName());
    //获取拦截的方法
    System.out.println(((HandlerMethod) handler).getMethod().getName());
    return false;
  }

  //请求之后，控制器中抛出了异常的话就不会执行
  @Override
  public void postHandle(HttpServletRequest httpServletRequest,
                         HttpServletResponse httpServletResponse,
                         Object handler, ModelAndView modelAndView)
      throws Exception {

  }

  // 请求之后调用，不管抛不抛出异常都会被调用.参数中异常如果被异常处理器调用的话就不会传入到参数中．
  @Override
  public void afterCompletion(HttpServletRequest httpServletRequest,
                              HttpServletResponse httpServletResponse,
                              Object handler,
                              Exception e)
      throws Exception {

  }
}
```
## 注册interceptor
继承`WebMvcConfigurerAdapter`接口,注入拦截器进行注册
```java
import com.micro.fast.security.demo.Ineterceptor.MyInterceptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;


/**
 * 注册外部的interceptor
 */
@Configuration
public class WebConfig  extends WebMvcConfigurerAdapter{
  @Autowired
  private MyInterceptor myInterceptor;

  /**
   * 添加拦截器
   * @param registry
   */
  @Override
  public void addInterceptors(InterceptorRegistry registry) {
    registry.addInterceptor(myInterceptor);
  }
}
```