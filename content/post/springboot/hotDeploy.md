---
title: idea+springboot+springboot-devTool实现开发时热部署
date: 2017-11-24
tags: ["idea","devTool"]
categories: ["springBoot"]
toc: true
---
idea+springboot+springboot-devTool实现开发时热部署
<!--more-->
## idea设置
    双击shift搜索Registry设置勾选compiler.automake.allow.when.app.running 
## maven中引入devTool依赖
```xml
<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-devtools</artifactId>
			<optional>true</optional>
</dependency>
```
## 运行springboot项目进行测试