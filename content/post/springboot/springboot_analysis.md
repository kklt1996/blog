+++
title='Springboot工作机制'
tags=[]
categories=["springBoot"]
date="2018-03-15T14:02:02+08:00"
toc=true
draft=false
+++

springBoot工作机制分析
<!--more-->
## @SpringBootApplication注解揭秘 
这个注解并不一定要修饰在主函数的类上，修饰其他类也是可以的。@SpringBootApplication注解相当于@Configuration @ComponentScan @EnableAutoConfiguration;只是这三个使用频率高所以就使用组合注解@SpringBootApplication表达了；@Configuration修饰一个类，表示的是这个类是IOC容器的配置类，EnableAutoConfiguration是借助import支持收集和注册特定场景相关的bean的定义，也可以看成是收集@Configuration修饰的类,自动配置的幕后英雄SpringFactoriesLoader,主要功能是加载META-INF/spring.factories下的配置文件，配置的格式是key=value 的标准的properties文件格式。根据EnableAutoConfiguration的全类名寻找对应的value,然后通过反射将这些类实例化为对应的标注@ConfigurationjavaConfig形式的Ioc容器配置类，然后加载到ioc容器中。这种机制就是实现了热插拔的自动配置。如果要先让自己的写的项目也有自动的配置的话在自己classpatch下定义META-INF/spring.factories然后设置key=value
key是EnableAutoConfiguration的包全名，value是自己配置类的包全名
## 深入探索SpringBoot的执行流程
如果忽略事件通知的扩展点，主要流程如下
[外链图片转存失败,源站可能有防盗链机制,建议将图片保存下来直接上传(img-qyFU7WmA-1585461586897)(http://note.youdao.com/yws/res/253/4D1568F527544E65B16A2DE84BD401CC)]
ApplicationListener是spring框架对java中实现的监听者模式的一种框架实现，但是ApplicationContextRunInitializer是不一样的，我们一般不会需要自己定义ApplicationContextRunInitializer。我们可以用过SpingApplication.addListener添加自定义的ApplicationListener.或者在META-INF/spring.factories文件下设置key 为ApplicationListener.全名的键值
## 调整自动配置的顺序
可以再@Configuration 修饰的Ioc配置类上添加AutoConfigureAfter(另一个配置类.class)，AutoConfigureBefore(另一个配置类.class)。除此之外springboot还为我们提供了其他一些诸多的条件化装配bean的配置类