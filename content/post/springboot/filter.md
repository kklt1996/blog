---
title: SpringBoot中定义Filter
date: 2017-10-21
tags: ["filter","intercept"]
categories: [springBoot]
toc: true
---
本篇文章中将介绍如何在springBoot中定义Filter
<!--more-->
## 创建Filter类
实现`Filter接口`拦截所有路径，filter拦截器不能知道filter是哪个controller处理的请求，只能获取请求中的信息．
```java
import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import java.io.IOException;

/**
 * 拦截所有服务的请求信息,filter拦截器不能知道filter是哪个controller处理的请求，只能获取请求中的信息
 */
@WebFilter(urlPatterns = {"/**"})
public class MyFilter implements Filter {

  @Override
  public void init(FilterConfig filterConfig) throws ServletException {

  }

  @Override
  public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
    //通过filter
    filterChain.doFilter(servletRequest,servletResponse);
  }

  @Override
  public void destroy() {

  }
}
```
## 注册第三方的Filter
注册第三方的Filter或者定义自定义Filter
```java
import com.micro.fast.security.demo.Ineterceptor.MyInterceptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;


/**
 * 注册外部的filter,
 */
@Configuration
public class WebConfig  extends WebMvcConfigurerAdapter{

  @Bean
  public FilterRegistrationBean myFilter(){
     FilterRegistrationBean filterRegistrationBean = new FilterRegistrationBean();
     filterRegistrationBean.setFilter(new MyFilter());
     String[]  urls = new String[]{"/**"};
    filterRegistrationBean.addUrlPatterns(urls);
    return filterRegistrationBean;
  }
}

```
