---
title: SpringBoot中定义aspect
date: 2017-10-21
tags: ["aspect","intercept"]
categories: ["springBoot"]
toc: true
---
本篇文章中将介绍如何在springBoot中定义aspect,拦截请求
<!--more-->
### 定义切面拦截请求
通知中能获取请求的方法中的参数
```java
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class MyAspect {

  @Pointcut("execution(* com.micro.fast.security.demo.controller.*(..))")
  public void pointcut(){

  }
  @Around("pointcut()")
  public Object around(ProceedingJoinPoint point){
    Object[] args = point.getArgs();
    try {
      //被拦截的方法
      Object object = point.proceed();
    } catch (Throwable throwable) {
      throwable.printStackTrace();
    }
    return null;
  }
}
```
##　拦截的顺序
拦截顺序Filter->Interceptor->ControllerAdvice->Aspect->Controller 发生异常的时候是反向的传递.ControllerAdvice处理的异常不
会传递到Interceptor的afterCompletion方法之中