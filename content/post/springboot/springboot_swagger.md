+++
title='Springboot集成Swagger文档'
tags=['swagger']
categories=["springBoot"]
date="2018-03-15T14:02:02+08:00"
toc=true
draft=false
+++

springboot集成swagger文档
<!--more-->
## 引入相关依赖
```xml
<dependency>
    <groupId>io.springfox</groupId>
    <artifactId>springfox-swagger2</artifactId>
    <version>2.6.1</version>
</dependency>
<dependency>
    <groupId>io.springfox</groupId>
    <artifactId>springfox-swagger-ui</artifactId>
    <version>2.6.1</version>
</dependency>
```
## 开启swagger服务，进行详细的配置
```java
package com.kklt.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.ResponseEntity;
import org.springframework.web.context.request.async.DeferredResult;
import springfox.documentation.builders.ApiInfoBuilder;
import static com.google.common.base.Predicates.or;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import static springfox.documentation.builders.PathSelectors.regex;

/**
 * Created by kklt on 2017/7/13.
 * SwaggerConfig
 */
@Configuration
@EnableSwagger2
public class Swagger2Config {
//设置一个组
    @Bean
    public Docket testApi() {
        return new Docket(DocumentationType.SPRING_WEB)
                .groupName("portalUser")
                .genericModelSubstitutes(DeferredResult.class)
                .genericModelSubstitutes(ResponseEntity.class)
                .useDefaultResponseMessages(false)
                .forCodeGeneration(true)
                .pathMapping("/")// base，最终调用接口后会和paths拼接在一起
                .select()
                .paths(or(regex("/user/.*")))//需要进行写入文档的接口
                .build()
                .apiInfo(portalUserApiInfo());
    }
//对于一个组头信息的描述
    private ApiInfo portalUserApiInfo() {
        return new ApiInfoBuilder()
                .title("Tesco")//大标题
                .description("前台用户接口")//详细描述
                .version("1.0")//版本
                .termsOfServiceUrl("NO terms of service")
                .contact(new Contact("李守余", "http://www.baidu.com", "183327xxxx0@163.com"))//作者
                .license("The Apache License, Version 2.0")
                .licenseUrl("http://www.apache.org/licenses/LICENSE-2.0.html")
                .build();
    }
}
```
访问 http://localhost:8080/swagger-ui.html